import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/views')
    },
    {
      path: '/login',
      component: () => import('@/views/login'),
      meta: {title: '登录'}
    }
  ]
})
