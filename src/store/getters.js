const getters = {
  token: state => state.user.token,
  device: state => state.app.device
}

export default getters
