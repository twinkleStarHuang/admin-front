import { getToken } from '@/utils/auth' //, setToken, removeToken

const user = {
  state: {
    token: getToken(),
    refreshToken: ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_REFRESHTOKEN: (state, refreshToken) => {
      state.refreshToken = refreshToken
    }
  }
}

export default user
