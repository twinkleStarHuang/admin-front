import Vue from 'vue'
import Cookies from 'js-cookie'

// A modern alternative to CSS resets
import 'normalize.css/normalize.css'

import App from './App'
import router from './router'
import store from './store'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'// progress bar style

import '@/styles/index.scss' // global css

import '@/icons' // icon

import ResizeMixin from '@/mixins/ResizeHandler'

Vue.mixin(ResizeMixin)

Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

router.beforeEach((to, from, next) => {
  NProgress.start()

  if(to.meta.title) {
    document.title = to.meta.title
  }
  next();
})

router.afterEach(() => {
  NProgress.done();
})
