
import 'iview/dist/styles/iview.css'
import Vue from 'vue'

import { Divider } from 'iview';
Vue.component('Divider', Divider);

export default Divider